<?php
namespace Controller;
require "Utils/Session.php";

use Model\Users;
use Symfony\Component\Translation\Dumper\PoFileDumper;
use Utils\CustomerData;
use Utils\Session;

class TestController
{
    private $templates;
    private $session;
    public function __construct()
    {
        $this->session = new Session();
        $this->templates = new \League\Plates\Engine( 'views', "php");
    }

    public function home()
    {
        $Users = [];
        if($this->session->isLogged()){
            $_POST["dataForm"] = json_encode(["word" => $_SESSION["last"]]);
            $_POST["fromPhp"] = true;
            $Users = $this->findUsers();
        }
        echo $this->templates->render("home", ['sesion' => $this->session->isLogged(), 'users' => json_encode($Users), 'last' => (isset($_SESSION["last"]) ? $_SESSION["last"] : '' )]);


    }

    public function login( $dataLogin = null )
    {

        if(count($_POST) > 0){
            if(count($_POST) > 0){ $dataLogin = json_decode($_POST["dataForm"]); }
            if($this->validateUser($dataLogin)){
                if(count($_POST) > 0){ echo json_encode(['response' => 'OK']); }
            } else {
                if(count($_POST) > 0){ echo json_encode(['response' => 'NO_OK']); }
            }
        } else {
            if($this->session->isLogged()){
                $this->home();
            } else {
                echo $this->templates->render("login");
            }
        }

    }

    public function validateUser($dataLogin)
    {
        $User = Users::where('email', '=', $dataLogin->user)
            ->where('password', '=', md5($dataLogin->passwd))
            ->first();
        if($User){
            $this->session->openSession($User);
            return true;
        }
        return false;
    }

    public function register()
    {

        if(count($_POST) > 0){
            if($this->saveUser($_POST["dataForm"])){
                $data = json_decode($_POST["dataForm"]);
                $_POST["dataForm"] = json_encode(['user' => $data->email, 'passwd' => $data->password]);
                $this->login();
            }
        } else {
            if($this->session->isLogged()){
                $this->home();
            } else {
                echo $this->templates->render("register");
            }
        }

    }


    public function findDocument()
    {
        $request = $_REQUEST;
        $empty = ['name' => '', 'document' => '', 'email' => '', 'country' => '' ];
        if(isset($request["documento"]) && !empty($request["documento"])){
            if(isset($request["origin"]) && $request["origin"] == "extern"){
                $customer = new CustomerData();
                $response = $customer->getCustomerData();
                $finded = false;
                foreach ($response as $items) {
                    foreach ($items as $item) {
                        if($item["cedula"] == $request["documento"]){
                            $finded = true;
                            echo json_encode(['name' => $item["primer_nombre"]." ".$item["apellido"], 'document' => $item["cedula"], 'email' => $item["correo"], 'country' => $item["pais"] ]);
                        }
                    }
                }
                if(!$finded){
                    echo json_encode($empty);
                }
            } else {
                if($User = Users::where('document', '=', $request["documento"])->first()){
                    echo json_encode($User);
                } else {
                    echo json_encode($empty);
                }
            }
        } else {
            echo json_encode($empty);
        }


    }

    public function saveUser($dataForm)
    {
        $dataForm = json_decode($dataForm);
        $User = new Users();
        $User->name = $dataForm->name;
        $User->document = $dataForm->document;
        $User->email = $dataForm->email;
        $User->country = $dataForm->country;
        $User->password = md5($dataForm->password);
        $User->save();
        return true;
    }


    public function findUsers()
    {
        $dataForm = json_decode($_POST["dataForm"]);
        if(!empty($dataForm->word)){
            $Users = Users::where(function ($query) use ($dataForm) {
                return $query->whereRaw('lower(email) like (?)', ["%{$dataForm->word}%"])
                    ->orWhereRaw('lower(name) like (?)', ["%{$dataForm->word}%"]);
            })->paginate();
            $export = [];
            foreach ($Users as $user){
                $export[] = ["name" => $user->name, "document" => $user->document, "email" => $user->email, "country" => $user->country ];
            }
            $userSearch = Users::find($_SESSION["userId"]);
            $userSearch->last = $dataForm->word;
            $userSearch->save();
            $_SESSION["last"] = $dataForm->word;
            if(!isset($_POST["fromPhp"])){
                echo json_encode($export);
            } else {
                return $export;
            }
        }

        return false;
    }



    public function logout()
    {
        $this->session->logout();
        echo json_encode(['response' => 'OK']);
    }

    public function notFound()
    {

        echo $this->templates->render("404");

    }
}