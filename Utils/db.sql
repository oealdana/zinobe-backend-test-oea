--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL COMMENT 'User Id',
  `name` varchar(200) NOT NULL COMMENT 'User Name',
  `document` varchar(200) NOT NULL COMMENT 'User Document',
  `email` varchar(200) NOT NULL COMMENT 'User Email',
  `country` varchar(200) NOT NULL COMMENT 'User Country',
  `password` varchar(50) NOT NULL COMMENT 'User Password',
  `last` varchar(100) NOT NULL COMMENT 'Last search',
  `created` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'User Registration Date'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Users Table';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_document` (`document`),
  ADD UNIQUE KEY `unique_email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'User Id';
COMMIT;