<?php


namespace Utils;


use GuzzleHttp\Client;

class CustomerData
{

    public function getCustomerData()
    {
        $client = new Client(['base_uri' => 'http://www.mocky.io/v2/']);
        $response = $client->request('GET', '5d9f38fd3000005b005246ac');
        $response = json_decode($response->getBody()->getContents(), true);
        return $response;
    }

}