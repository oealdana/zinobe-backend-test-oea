<?php
namespace Utils;

class Session
{

    public function __construct()
    {
        session_start();
    }

    public function isLogged()
    {
        $valid_session = isset($_SESSION['id']) ? $_SESSION['id'] === session_id() : FALSE;
        if (!$valid_session) {
            return false;
        }
        return true;
    }

    public function openSession($dataUser)
    {
        $_SESSION["userId"] = $dataUser->id;
        $_SESSION["user"] = $dataUser->email;
        $_SESSION["name"] = $dataUser->name;
        $_SESSION["last"] = $dataUser->last;
        $_SESSION["id"] = session_id();
    }

    public function logout()
    {
        session_destroy();
    }
}