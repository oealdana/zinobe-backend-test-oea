<?php
$this->layout('layaut', ['title' => 'Home Test']) ?>

<div class="text-center">
<h1>Directorio De Personas</h1>
<?php
if($this->e($sesion)){
    echo "<p>Hola , {$_SESSION["name"]}   &nbsp;&nbsp;&nbsp;&nbsp;<a href='javascript:;' onclick='logout()'>Salir</a></p>";
    echo "<form class='form-inline justify-content-center' name='formSearch' onsubmit='return searchUsers();'><input class='form-control' type='text' id='search' value='{$last}'><input type='submit' class='btn btn-primary my-2 my-sm-0' value='Buscar'></form>";
    if($this->e($users)){
        echo "<br><div id='responseSearch'>";
        $users = json_decode($users);
        if(is_array($users) && count($users) > 0){
            echo '<table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Documento</th>
                      <th scope="col">Nombre</th>
                      <th scope="col">Email</th>
                      <th scope="col">Pais</th>
                    </tr>
                  </thead>
                  <tbody>';
            foreach ($users as $user) {
                    echo '<tr>
                          <th>'.$user->document.'</th>
                          <td>'.$user->name.'</td>
                          <td>'.$user->email.'</td>
                          <td>'.$user->country.'</td>
                        </tr>';
            };

            echo "        </tbody>
                    </table>";
            } else {
                echo "No se encontraron registros";
            }


        echo "</div>";
    } else {
        echo "<br><div id='responseSearch'></div>";
    }
} else {
    echo "<a href='index.php/login'>Ingresar</a> &nbsp;&nbsp;&nbsp;&nbsp; <a href='index.php/register'>Registrarse</a>";
}
?>
</div>
