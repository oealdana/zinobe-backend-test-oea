<?php
$this->layout('layaut', ['title' => 'Register User Test']) ?>
<div id="login">
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <form id="login-form" class="form" action="" name="formRegister" method="post" onsubmit="return registerUser();">
                        <h3 class="text-center text-info">Regitrarme</h3>
                        <div class="form-group">
                            <label for="username" class="text-info">Documento:</label><br>
                            <input type="text" required name="documento" id="documento" class="form-control" onchange="findUser()">
                            <a href="http://www.mocky.io/v2/5d9f38fd3000005b005246ac" target="_blank">help</a>
                            <div id="moreFields"></div>
                        </div>
                        <div class="form-group">
                            <input type="submit" id="register" class="btn btn-info btn-md" value="Ingresar" disabled>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>