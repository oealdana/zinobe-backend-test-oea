<?php
$this->layout('layaut', ['title' => 'Login Test']) ?>

<div id="login">
    <div class="container">
        <div id="login-row" class="row justify-content-center align-items-center">
            <div id="login-column" class="col-md-6">
                <div id="login-box" class="col-md-12">
                    <form id="login-form" class="form" action="" method="post" onsubmit="return login()">
                        <h3 class="text-center text-info">Ingresar</h3>
                        <div class="form-group">
                            <label for="username" class="text-info">Usuario:</label><br>
                            <input type="text" name="username" id="username" class="form-control">
                        </div>
                        <div class="form-group">
                            <label for="password" class="text-info">Contraseña:</label><br>
                            <input type="password" name="password" id="password" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" class="btn btn-info btn-md" value="Ingresar">
                        </div>
                        <div id="register-link" class="text-right">
                            <a href="/test-zinobe/index.php/register" class="text-info">Registrarme</a> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a href="/test-zinobe" class="text-info">Volver</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
