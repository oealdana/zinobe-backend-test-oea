<?php
require 'vendor/autoload.php';
require 'config/database.php';
require 'Controller/TestController.php';
use Controller\TestController;


$request = $_SERVER['PHP_SELF'];

// makeshift router
switch ($request) {
    case '/test-zinobe/' :
        $init = new \Controller\TestController();
        $init->home();
        break;
    case '/test-zinobe/index.php' :
        $init = new \Controller\TestController();
        $init->home();
        break;
    case '/test-zinobe/index.php/login' :
        $init = new \Controller\TestController();
        $init->login();
        break;
    case '/test-zinobe/index.php/register' :
        $init = new \Controller\TestController();
        $init->register();
        break;
    case '/test-zinobe/index.php/findDocument' :
        $init = new \Controller\TestController();
        $init->findDocument();
        break;
    case '/test-zinobe/index.php/findUsers' :
        $init = new \Controller\TestController();
        $init->findUsers();
        break;
    case '/test-zinobe/index.php/logout' :
        $init = new \Controller\TestController();
        $init->logout();
        break;
    default:
        http_response_code(404);
        $init = new \Controller\TestController();
        $init->notFound();
        break;
}
