# Zinobe Backend Test Oea

Repositorio para el test aplicando al cargo de programador Back End PHP

Install

* Clonar el proyecto con un nombre de directorio especifico en el Document Root "test-zinobe"

git clone https://gitlab.com/oealdana/zinobe-backend-test-oea.git test-zinobe

* Ejecutar el comando "composer install" dentro del directorio del proyecto

* Crear la base de datos y luego ejecutar el archivo /Utils/db.sql

* Configurar el acceso a la base de datos en el archivo /config/database.php

* Ejecutar la aplicacion.