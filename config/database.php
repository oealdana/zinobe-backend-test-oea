<?php

use Illuminate\Database\Capsule\Manager as capsule;

$capsile = new Capsule;

$capsile->addConnection([
    'driver'    => 'mysql',
    'host'      => 'localhost',
    'database'  => 'users_directory',
    'username'  => 'root',
    'password'  => '',
    'chasrset'  => 'utf8',
    'collation' => 'utf8_unicode_ci',
    'prefix'    => ''
]);

$capsile->bootEloquent();