function findUser() {
    // Find in local database
    var documento = $('#documento').val();
    $("#moreFields").html("");
    $(':input[type="submit"]').prop('disabled', true);
    $.ajax({
        data: {"documento": documento},
        type: "GET",
        dataType: "json",
        url: "/test-zinobe/index.php/findDocument",
    })
    .done(function (data, textStatus, jqXHR) {

        // If the ID exists in our DB
        if(data.name.length > 0){
            alert("Ya existe un usuario con ese documento. ("+data.name+")");
        } else {
            // Find in a extern repository
            $.ajax({
                data: {"documento": documento, "origin": "extern"},
                type: "GET",
                dataType: "json",
                url: "/test-zinobe/index.php/findDocument",
            })
                .done(function (response, textStatus, jqXHR) {
                    asignRegisterForm(response);
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    if (console && console.log) {
                        console.log("La solicitud a fallado: " + textStatus);
                    }
                });
        }


    })
    .fail(function (jqXHR, textStatus, errorThrown) {
        if (console && console.log) {
            console.log("La solicitud a fallado: " + textStatus);
        }
    });
}


function login(){
    var obj = {user: $('#username').val(), passwd: $('#password').val()};
    $.ajax({
        data: {"dataForm": JSON.stringify(obj)},
        type: "POST",
        dataType: "json",
        url: "/test-zinobe/index.php/login"
    })
        .done(function (response, textStatus, jqXHR) {
            if(response.response == 'OK'){
                location.href = "/test-zinobe";
            } else {
                alert('Ocurrio un error en la autenticacion.');
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            if (console && console.log) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });
    return false;
}


function asignRegisterForm(data){
    $("#moreFields").html("");
    var fields = "<label for='name' class='text-info'>Nombre:</label><br>\n" +
        "<input type='text' required name='name' id='name' minlength='3' class='form-control' value='"+data.name+"'>\n"+
        "<label for='country' class='text-info'>Pais:</label><br>\n"+
        "<select id='country' required><option value=''>Seleccione Pais</option></option></select><br>\n" +
        "<label for='email' class='text-info'>Correo:</label><br>\n" +
        "<input type='email' required name='email' id='email' class='form-control' value='"+data.email+"'>\n" +
        "<label for='password' class='text-info'>Contraseña:</label><br>\n" +
        "<input type='password' required minlength='6' pattern='^(?=.*[A-Za-z])(?=.*\\d)[A-Za-z\\d]{6,}$' name='password' id='password' class='form-control'>\n" +
        "";
    $("#moreFields").html(fields);
    getCountries(data.country);
    //$("#formRegister").onsubmit;
    $(':input[type="submit"]').prop('disabled', false);
}

function getCountries(country){
    var pais = country;
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "https://pkgstore.datahub.io/core/country-list/data_json/data/8c458f2d15d9f2119654b29ede6e45b8/data_json.json",
    })
        .done(function (response, textStatus, jqXHR) {
            for (var clave in response){
                if(response[clave].Name == pais){
                    $("#country").append("<option selected value='"+response[clave].Name+"'>" + response[clave].Name + "</option>");
                } else {
                    $("#country").append("<option value='"+response[clave].Name+"'>" + response[clave].Name + "</option>");
                }
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            if (console && console.log) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });
}

function registerUser(){
    var obj ={document: $('#documento').val(), name: $('#name').val(), country: $('#country').val(), email: $('#email').val(), password: $('#password').val()};
    $.ajax({
        data: {"dataForm": JSON.stringify(obj)},
        type: "POST",
        dataType: "json",
        url: "/test-zinobe/index.php/register"
    })
        .done(function (response, textStatus, jqXHR) {
            if(response.response == 'OK'){
                location.href = "/test-zinobe";
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            if (console && console.log) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });

    return false;
}


function searchUsers(){
    var obj ={word: $('#search').val()};
    $("#responseSearch").html('');
    $.ajax({
        data: {"dataForm": JSON.stringify(obj)},
        type: "POST",
        dataType: "json",
        url: "/test-zinobe/index.php/findUsers"
    })
        .done(function (response, textStatus, jqXHR) {
            if(response.length > 0){
                var table = '<table class="table">' +
                    '           <thead>' +
                    '               <tr>\n' +
                    '                    <th scope="col">Documento</th>\n' +
                    '                    <th scope="col">Nombre</th>\n' +
                    '                    <th scope="col">Email</th>\n' +
                    '                    <th scope="col">Pais</th>\n' +
                    '               </tr>\n' +
                    '           </thead>\n' +
                    '           <tbody>';
                for (var user in response) {
                    console.log(user);
                    table += '<tr>\n' +
                        '                    <td>'+response[user].document+'</td>\n' +
                        '                    <td>'+response[user].name+'</td>\n' +
                        '                    <td>'+response[user].email+'</td>\n' +
                        '                    <td>'+response[user].country+'</td>\n' +
                        '                    </tr>\n';
                }
                table += '</tbody>\n' +
                    '   </table>';

                $("#responseSearch").html(table);
            } else{
                $("#responseSearch").html("No se encontraron registros.");
            }

        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            if (console && console.log) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });
    return false;
}

function logout(){
    $.ajax({
        type: "GET",
        dataType: "json",
        url: "/test-zinobe/index.php/logout"
    })
        .done(function (response, textStatus, jqXHR) {
            if(response.response == 'OK'){
                location.href = "/test-zinobe";
            }
        })
        .fail(function (jqXHR, textStatus, errorThrown) {
            if (console && console.log) {
                console.log("La solicitud a fallado: " + textStatus);
            }
        });
}
